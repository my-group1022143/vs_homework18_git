#include <iostream>
#include <algorithm>

class Player
{
public:
    Player() = default;
    Player(const std::string& name, int score) : name(name), score(score) {}
    std::string name;
    int score;
};

int main()
{
    setlocale(LC_ALL, "Russian");
    system("chcp 1251 > nul");
    int numPlayers;
    std::cout << "������� ���������� �������: ";
    std::cin >> numPlayers;
    Player* players = new Player[numPlayers];

    for (int i = 0; i < numPlayers; ++i)
    {
        std::string name;
        int score;
        std::cout << "������� ��� ������ " << i + 1 << ": ";
        std::cin >> name;
        std::cout << "������� ���� ������ " << i + 1 << ": ";
        std::cin >> score;
        players[i] = Player(name, score);
    }

    for (int i = 1; i < numPlayers; ++i)
    {
        for (int j = 0; j < numPlayers - i; ++j)
        {
            if (players[j].score < players[j + 1].score)
            {
                std::swap(players[j], players[j + 1]);
            }
        }
    }

    for (int i = 0; i < numPlayers; ++i)
    {
        std::cout << players[i].name << " " << players[i].score << std::endl;
    }

    delete[] players;

    return 0;
}